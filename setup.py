from setuptools import setup

__version__ = "1.0.1"

setup(
   name="semantic_release_package",
   version=__version__
)
